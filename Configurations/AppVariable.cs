﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Vodji.Atlas.Data;

namespace Vodji.Atlas.Configurations
{
    [Keyless]
    public class AppVariable
    {
        [Key]
        public string Key { get; set; }
        public string Value { get; set; }

        public static bool TryGetValue(string key, out string value)
        {
            value = default;
            using (var sql = new SQLConnection())
            {
                var sqlResults = sql.appVariables.Where(u => u.Key.ToLower().Equals(key.ToLower())).Select(s => s.Value).AsEnumerable();
                if (sqlResults.Any())
                {
                    value = sqlResults.ElementAt(0);
                    return true;
                }
            }

            return false;
        }

        public static bool SetValue(string key, object value) => SetValueAsync(key, value).Result;
        public static async Task<bool> SetValueAsync(string key, object value)
        {
            using (var sql = new SQLConnection())
            {
                var appVariable = await sql.appVariables.FirstOrDefaultAsync(u => u.Key.ToLower().Equals(key));
                if(appVariable is null)
                {
                    sql.appVariables.Add(new AppVariable { Key = key, Value = value.ToString() });
                    return await sql.SaveChangesAsync() > 0;
                }

                appVariable.Value = value.ToString();
                sql.appVariables.Update(appVariable);
                return await sql.SaveChangesAsync() > 0;
            }
        }
    }
}

/*
var size = Marshal.SizeOf(your_object);
// Both managed and unmanaged buffers required.
var bytes = new byte[size];
var ptr = Marshal.AllocHGlobal(size);
// Copy object byte-to-byte to unmanaged memory.
Marshal.StructureToPtr(your_object, ptr, false);
// Copy data from unmanaged memory to managed buffer.
Marshal.Copy(ptr, bytes, 0, size);
// Release unmanaged memory.
Marshal.FreeHGlobal(ptr);

And to convert bytes to object:

var bytes = new byte[size];
var ptr = Marshal.AllocHGlobal(size);
Marshal.Copy(bytes, 0, ptr, size);
var your_object = (YourType)Marshal.PtrToStructure(ptr, typeof(YourType));
Marshal.FreeHGlobal(ptr);

*/