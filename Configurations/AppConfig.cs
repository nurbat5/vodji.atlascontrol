﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Vodji.Atlas.AtlasCMD;
using Vodji.Atlas.Data;
using Vodji.Shared.Utils;

namespace Vodji.Atlas.Configurations
{
    public class AppConfig
    {
        public Guid AppConfigId { get; set; } = Guid.NewGuid();
        public string ApplicationName { get; set; } = "MyFirstCluster";
        public string ServerArguments { get; set; } = "{mapName}" +
            "?{gridLocation}" +
            "?AltSaveDirectoryName={saveDir}" +
            "?MaxPlayers={maxPlayers}" +
            "?ReservedPlayerSlots={reservedSlots}" +
            "?RCONEnabled={rconEnabled}" +
            "?RCONPort={rconPort}" +
            "?QueryPort={queryPort}" +
            "?Port={gamePort}" +
            "?SeamlessIP={seamlessIP}" +
            //"?SeamlessPort={seamlessPort}" +
            "?ServerAdminPassword={serverAdminPassword}" +
            " -log -server";
        public string GameInstalled { get; set; } = Path.GetFullPath(GameConfig.INSTALL_DIR);
        public string ServerAdminPassword { get; set; } = Guid.NewGuid().GetHashCode().ToString();
        public string SeamlessIP { get; set; } = HardwareAdress.GetWebIP();
        public int GamePort { get; set; } = 3000;
        public int QueryPort { get; set; } = 35000;
        public int RconPort { get; set; } = 40000;
        public int SeamlessPort { get; set; } = 27000;
        public int MaxPlayers { get; set; } = 100;
        public int ReservedSlots { get; set; } = 10;

        public ICollection<AppServer> AppServers
        {
            get
            {
                using(var sql = new SQLConnection())
                {
                    return sql.AppServers.Where(u => u.AppConfigID.Equals(AppConfigId)).ToList();
                }
            }
        }
    }
}
