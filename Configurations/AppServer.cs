﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Vodji.Atlas.AtlasCMD.ShooterGame.Models;

namespace Vodji.Atlas.Configurations
{
    public enum Map
    {
        Ocean
    }

    public class AppServer
    {
        public Guid AppServerId { get; set; }
        public Guid AppConfigID { get; set; }
        public Map Map { get; set; } = Map.Ocean;
        public string Name { get; set; }
        public string GridName { get; set; }
        public string SeamlessIP { get; set; }
        public string CmdLineArguments { get; set; }
        public string ServerAdminPassword { get; set; }

        public int? ReservedSlots { get; set; }
        public int? SeamlessPort { get; set; }
        public int? MaxPlayers { get; set; }
        public int? QueryPort { get; set; }
        public int? GamePort { get; set; }
        public int? RconPort { get; set; }
        public int? PID { get; set; }

        public bool RconEnabled { get; set; } = true;
        public bool IsHomeServer { get; set; } = false;
        
        [NotMapped]
        public Server Server { get; set; }
    }
}
