﻿using System.Diagnostics;
using System.Threading.Tasks;
using Vodji.Atlas.AtlasCMD;

namespace Vodji.Atlas.Configurations.Extensions
{
    public static class AppServerExtension
    {
        public static string GetCmdLineArguments(this AppServer appServer)
            => string.IsNullOrWhiteSpace(appServer.CmdLineArguments) ? GameConfig.AppConfig.ServerArguments : appServer.CmdLineArguments;
        public static string GetServerAdminPassword(this AppServer appServer)
            => string.IsNullOrWhiteSpace(appServer.ServerAdminPassword) ? GameConfig.AppConfig.ServerAdminPassword : appServer.ServerAdminPassword;
        public static string GetSeamlessIP(this AppServer appServer)
            => string.IsNullOrWhiteSpace(appServer.SeamlessIP) ? GameConfig.AppConfig.SeamlessIP : appServer.SeamlessIP;
       
        public static int GetMaxPlayers     (this AppServer appServer) => appServer.MaxPlayers      ?? GameConfig.AppConfig.MaxPlayers;
        public static int GetReservedSlots  (this AppServer appServer) => appServer.ReservedSlots   ?? GameConfig.AppConfig.ReservedSlots;
        public static int GetQueryPort      (this AppServer appServer) => appServer.QueryPort       ?? GameConfig.GetPort(appServer.Server, PortMode.QueryPort);
        public static int GetGamePort       (this AppServer appServer) => appServer.GamePort        ?? GameConfig.GetPort(appServer.Server, PortMode.GamePort);
        public static int GetRconPort       (this AppServer appServer) => appServer.RconPort        ?? GameConfig.GetPort(appServer.Server, PortMode.RconPort);
        public static int GetSeamlessPort   (this AppServer appServer) => appServer.SeamlessPort    ?? GameConfig.GetPort(appServer.Server, PortMode.SeamlessPort);

        public static int GetGridX(this AppServer appServer) => LETTERS.IndexOf(appServer.GridName[0]);
        public static int GetGridY(this AppServer appServer) => int.Parse(appServer.GridName[1].ToString()) - 1;

        public const string LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
}
