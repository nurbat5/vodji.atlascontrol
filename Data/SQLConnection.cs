﻿using Microsoft.EntityFrameworkCore;
using Vodji.Atlas.Configurations;
using Vodji.Atlas.Data.Models;
using Vodji.Shared.Data.SQL;
using Vodji.Shared.Data.SQL.Provider;

namespace Vodji.Atlas.Data
{
    public class SQLConnection : SQLContext
    {
        public DbSet<AppServer> AppServers { get; set; }
        public DbSet<AppConfig> appConfigs { get; set; }
        public DbSet<AppVariable> appVariables { get; set; }

        public SQLConnection() : base(new SQLiteProvider("Vodji.Atlas.Configuration.db"))
        {
            if (Database.EnsureCreated())
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppVariable>().HasKey(u => u.Key);
            //modelBuilder.Entity<AppServer>().HasOne(u => u.AppConfig).WithMany(u => u.AppServers).HasForeignKey(u => u.AppConfigId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
