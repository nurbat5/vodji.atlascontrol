using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Vodji.Atlas.Data;
using System.Linq;
using Vodji.Atlas.Configurations;
using Vodji.Atlas.AtlasCMD;
using System.IO;
using Vodji.Atlas.AtlasCMD.ShooterGame;
using Vodji.Atlas.SteamCMD;

namespace Vodji.Atlas
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Verify Application...");
            // Install AppConfig
            using (var sql = new SQLConnection())
            {
                if (sql.appConfigs.Any())
                {
                    if (!AppVariable.TryGetValue("appConfigId", out var appConfigId))
                    {
                        Console.WriteLine("[0] Failed Application...");
                        GameConfig.AppConfig = sql.appConfigs.AsEnumerable().Reverse().Take(1).ElementAt(0);
                        if (!AppVariable.SetValue("appConfigId", GameConfig.AppConfig.AppConfigId))
                        {
                            Console.WriteLine("[1] Failed Application...");
                            Console.ReadKey();
                            Environment.Exit(0);
                        }
                    }

                    GameConfig.AppConfig = sql.appConfigs.FirstOrDefault(u => u.AppConfigId.Equals(Guid.Parse(appConfigId)));
                    if (GameConfig.AppConfig is null)
                    {
                        Console.WriteLine("[2] Failed Application...");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }
                }
                else
                {
                    GameConfig.AppConfig = new();

                    sql.appConfigs.AddAsync(GameConfig.AppConfig);
                    if (sql.SaveChanges() == 0)
                    {
                        Console.WriteLine("[3] Failed Application...");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }

                    if(!AppVariable.SetValue("appConfigId", GameConfig.AppConfig.AppConfigId))
                    {
                        Console.WriteLine("[4] Failed Application...");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }
                }
            }

            if (!AppVariable.TryGetValue("UseUrls", out var useUrls))
                AppVariable.SetValue("UseUrls", $"http://localhost:5000,http://{GameConfig.AppConfig.SeamlessIP}:5001");

            // Install SteamCMD & AtlasCMD
            SteamCommand.Verify();
            if (args.Contains("-validate")) SteamCommand.InstallGame(false);
            else SteamCommand.InstallGame();

            // Install GameConfig
            if (!File.Exists(GameConfig.GetInstallDir() + GameConfig.SERVER_GRID))
            {
                Console.WriteLine("[4] Failed Application...");
                Console.WriteLine(" > " + GameConfig.GetInstallDir() + GameConfig.SERVER_GRID);
                Console.ReadKey();
                Environment.Exit(0);
            }

            GameConfig.ServerGrid = ServerGrid.LoadAsync(GameConfig.GetInstallDir() + GameConfig.SERVER_GRID).Result;
            if (!File.Exists(GameConfig.GetInstallDir() + GameConfig.SERVER_ONLY))
            {
                GameConfig.ServerGrid.GenerateDatabaseConnections();
                GameConfig.ServerGrid.UpdateDatabaseAsync().Wait();
                GameConfig.ServerGrid.UpdateAsync().Wait();
            }

            Console.WriteLine();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
                {
                    if (AppVariable.TryGetValue("UseUrls", out var useUrls))
                    {
                        webBuilder.UseUrls(useUrls.Split(','));
                    }
                    webBuilder.UseStartup<Startup>();
                });
        }
    }
}