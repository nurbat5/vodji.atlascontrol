﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Threading.Tasks;
using Vodji.Atlas.AtlasCMD;

namespace Vodji.Atlas.SteamCMD
{
    public enum SteamCommandStatus
    {
        None,
        Downloading,
        Extracting,
        Verify,
        Finish
    }

    public static class SteamCommand
    {
        public const string PATH_ZIP = "SteamCMD/steamcmd.zip";
        public const string PATH_EXE = "SteamCMD/steamcmd.exe";
        public const string DOWNLOAD = "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip";

        public static string GetPathDir() => Path.GetDirectoryName(GetPathExe());
        public static string GetPathExe() => Path.GetFullPath(PATH_EXE);
        public static string GetPathZip() => Path.GetFullPath(PATH_ZIP);

        public static SteamCommandStatus CommandStatus { get; private set; } = SteamCommandStatus.None;
        public static void DownloadAsync()
        {
            Directory.CreateDirectory(GetPathDir());

            using (var webClient = new WebClient())
            {
                Console.WriteLine($"Downloading {DOWNLOAD}");
                CommandStatus = SteamCommandStatus.Downloading;
                webClient.DownloadFile(DOWNLOAD, GetPathZip());
            }

            Console.WriteLine($"Extracting {Path.GetFileName(GetPathZip())}..");
            CommandStatus = SteamCommandStatus.Extracting;
            ZipFile.ExtractToDirectory(GetPathZip(), GetPathDir());
            File.Delete(GetPathZip());
        }
        public static bool IsInstalled() => File.Exists(GetPathExe());
        public static void Verify()
        {
            if (!IsInstalled()) DownloadAsync();

            Console.WriteLine($"Verify SteamCMD..");
            CommandStatus = SteamCommandStatus.Verify;
            using (var steamProcess = new Process())
            {
                steamProcess.StartInfo.FileName = GetPathExe();
                steamProcess.StartInfo.UseShellExecute = false;
                steamProcess.StartInfo.CreateNoWindow = true;
                steamProcess.StartInfo.Arguments = "+exit";
                steamProcess.Start();
                steamProcess.WaitForExit();
            }
         
            CommandStatus = SteamCommandStatus.Finish;
        }

        public static void ValidateGame() => InstallGame(false);
        public static void InstallGame(bool install = true)
        {
            Console.WriteLine($"Verify Atlas..");
            Directory.CreateDirectory(GameConfig.GetInstallDir());

            var validate = install ? string.Empty : "validate";
            using (var gameServerProccess = new Process())
            {
                gameServerProccess.StartInfo.FileName = GetPathExe();
                gameServerProccess.StartInfo.UseShellExecute = false;
                gameServerProccess.StartInfo.CreateNoWindow = false;

                gameServerProccess.StartInfo.Arguments = $"+login anonymous +force_install_dir {GameConfig.GetInstallDir()} +app_update 1006030 {validate} +quit";
                gameServerProccess.Start();
                gameServerProccess.WaitForExit();
            }
        }
    }
}
