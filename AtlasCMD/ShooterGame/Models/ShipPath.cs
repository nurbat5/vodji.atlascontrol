﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class ShipPath
    {
        public List<Node> Nodes { get; set; }
        public int PathId { get; set; }
        public bool isLooping { get; set; }
        public string PathName { get; set; }
        public string AutoSpawnShipClass { get; set; }
        public int AutoSpawnEveryUTCInterval { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Include)]
        public bool autoSpawn { get; set; }

        //public Node GetNextNode(Node node)
        //{
        //    int idx = Nodes.IndexOf(node) + 1;

        //    if (idx == Nodes.Count)
        //        return Nodes[0];

        //    if (idx < 0 || idx > Nodes.Count)
        //        return null;

        //    return Nodes[idx];
        //}
        //public Node GetPrevNode(Node node)
        //{
        //    int idx = Nodes.IndexOf(node) - 1;

        //    if (idx == -1)
        //        return Nodes[Nodes.Count - 1];

        //    if (idx < 0 || idx >= Nodes.Count)
        //        return null;

        //    return Nodes[idx];
        //}
        //public bool DeleteNode(Node node)
        //{
        //    if (Nodes.Count <= 2) return false;

        //    int idx = Nodes.IndexOf(node);
        //    if(idx >= 0) Nodes.RemoveAt(idx);
        //    return true;
        //}
        //public Node AddNode(Node afterNode)
        //{
        //    int idx = Nodes.IndexOf(afterNode);
        //    if (idx >= 0)
        //    {
        //        var nextNode = GetNextNode(afterNode);
        //        float midX = (afterNode.worldX + nextNode.worldX) / 2;
        //        float midY = (afterNode.worldY + nextNode.worldY) / 2;
        //        var newNode = new Node() { worldX = midX, worldY = midY, rotation = 0 };
        //        Nodes.Insert(idx + 1, newNode);
        //    }

        //    return null;
        //}
    }
}
