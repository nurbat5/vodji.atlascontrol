﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.Json;
using Newtonsoft.Json;

namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class Server
    {
        [DeploymentConst]
        public int gridX { get; set; }
        [DeploymentConst]
        public int gridY { get; set; }
        [DeploymentOverride]
        public string MachineIdTag { get; set; }
        [DeploymentOverride]
        public string ip { get; set; }
        public string name { get; set; }
        [DeploymentOverride]
        public int port { get; set; }
        [DeploymentOverride]
        public int gamePort { get; set; }
        [DefaultValue(27000)]
        [DeploymentOverride]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public int seamlessDataPort { get; set; }
        public bool isHomeServer { get; set; }
        public string AdditionalCmdLineParams { get; set; }
        public Dictionary<string, string> OverrideShooterGameModeDefaultGameIni { get; set; }
        public int floorZDist { get; set; }
        public int utcOffset { get; set; }
        public int transitionMinZ { get; set; }
        public string GlobalBiomeSeamlessServerGridPreOffsetValues { get; set; }
        public string GlobalBiomeSeamlessServerGridPreOffsetValuesOceanWater { get; set; }
        public string OceanDinoDepthEntriesOverride { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string OceanEpicSpawnEntriesOverrideValues { get; set; }
        public string oceanFloatsamCratesOverride { get; set; }
        public string treasureMapLootTablesOverride { get; set; }
        public string oceanEpicSpawnEntriesOverrideTemplateName { get; set; }
        public string NPCShipSpawnEntriesOverrideTemplateName { get; set; }
        public string regionOverrides { get; set; }
        public float waterColorR { get; set; }
        public float waterColorG { get; set; }
        public float waterColorB { get; set; }
        public int skyStyleIndex { get; set; }
        public float serverIslandPointsMultiplier { get; set; }
        public SubLevel[] sublevels { get; set; }
        public DateTime lastModified { get; set; }
        public DateTime lastImageOverride { get; set; }
        public bool islandLocked { get; set; }
        public bool discoLocked { get; set; }
        public bool pathsLocked { get; set; }
        public List<string> extraSublevels { get; set; }
        public List<string> totalExtraSublevels { get; set; }
        public List<IslandInstance> islandInstances { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ServerCustomDatas1 { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ServerCustomDatas2 { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ClientCustomDatas1 { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ClientCustomDatas2 { get; set; }
        public List<DiscoZone> discoZones { get; set; }
        public List<SpawnRegion> spawnRegions { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string serverTemplateName { get; set; }

        //public string GetGridLocationHumanReadable()
        //{
        //    const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //    var Column = "";
        //    if (gridX >= letters.Length)
        //        Column += letters[gridX / letters.Length - 1];
        //    Column += letters[gridX % letters.Length];
        //    return Column + (gridY + 1);
        //}

        //public bool IsWorldPointInServer(PointF point, float gridSize) => GetWorldRect(gridSize).Contains(point);
        //public RectangleF GetWorldRect(float gridSize) => new RectangleF(new PointF(gridX * gridSize, gridY * gridSize), new SizeF(gridSize, gridSize));
        //public PointF WorldToRelativePoint(float gridSize, PointF worldPoint)
        //{
        //    PointF ServerWorldCenter = new PointF(gridX * gridSize + gridSize / 2, gridY * gridSize + gridSize / 2);
        //    return new PointF(worldPoint.X - ServerWorldCenter.X, worldPoint.Y - ServerWorldCenter.Y);
        //}
        //public PointF RelativeToWorldPoint(float gridSize, PointF relativePoint)
        //{
        //    PointF ServerWorldCenter = new PointF(gridX * gridSize + gridSize / 2, gridY * gridSize + gridSize / 2);
        //    return new PointF(relativePoint.X + ServerWorldCenter.X, relativePoint.Y + ServerWorldCenter.Y);
        //}
    }
}
