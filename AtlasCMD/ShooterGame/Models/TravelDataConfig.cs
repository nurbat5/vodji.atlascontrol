﻿namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class TravelDataConfig
    {
        public string BackupMode { get; set; } = "off";
        public int MaxFileHistory { get; set; } = 10; // shared log and tribe log
        public string HttpBackupURL { get; set; } = string.Empty;
        public string HttpAPIKey { get; set; } = string.Empty;
        public string S3URL { get; set; } = string.Empty;
        public string S3AccessKeyId { get; set; } = string.Empty;
        public string S3SecretKey { get; set; } = string.Empty;
        public string S3BucketName { get; set; } = string.Empty;
        public string S3KeyPrefix { get; set; } = string.Empty;
    }
}
