﻿namespace Vodji.Atlas.AtlasCMD.ShooterGame.Models
{
    public class SpawnerOverrideTemplate
    {
        public string Name { get; set; }
        public string NPCSpawnEntries { get; set; }
        public string NPCSpawnLimits { get; set; }
        public float MaxDesiredNumEnemiesMultiplier { get; set; }
    }
}
