﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Vodji.Atlas.AtlasCMD.ShooterGame.Models;
using Vodji.Atlas.Configurations;
using Vodji.Atlas.Configurations.Extensions;
using Vodji.Atlas.Data;

namespace Vodji.Atlas.AtlasCMD.ShooterGame.Extensions
{
    public static class ServerExtension
    {
        public static async Task<AppServer> CreateAppServerAsync(this Server server)
        {
            var appServer = GameConfig.AppConfig.AppServers.FirstOrDefault(u => u.GridName.Equals(server.GetGridName()));
            if(appServer is null)
            {
                using(SQLConnection sql = new())
                {
                    appServer = new AppServer
                    {
                        AppConfigID = GameConfig.AppConfig.AppConfigId,
                        GridName = server.GetGridName(),
                        Name = server.name,
                        IsHomeServer = server.isHomeServer
                    };

                    await sql.AddAsync(appServer);
                    if(sql.SaveChangesAsync() == default)
                    {
                        Console.WriteLine($"{DateTime.UtcNow} [FAIL] createAppServer {server.GetGridName()}");
                        return null;
                    }
                }
            }

            appServer.Server = server;
            return appServer;
        }

        public static async Task<bool> LaunchServerAsync(this Server server)
        {
            var appServer = await server.CreateAppServerAsync();
            if(appServer is not null)
            {
                var cmdArguments = appServer.GetCmdLineArguments();
                cmdArguments = cmdArguments.Replace("{mapName}",  appServer.Map.ToString());
                cmdArguments = cmdArguments.Replace("{serverAdminPassword}", appServer.GetServerAdminPassword());
                cmdArguments = cmdArguments.Replace("{seamlessIP}", string.IsNullOrWhiteSpace(server.ip) ? GameConfig.AppConfig.SeamlessIP : server.ip);
                cmdArguments = cmdArguments.Replace("{gridLocation}", "ServerX={serverY}?ServerY={serverX}");
                cmdArguments = cmdArguments.Replace("{serverY}", appServer.GetGridX().ToString());
                cmdArguments = cmdArguments.Replace("{serverX}", appServer.GetGridY().ToString());
                cmdArguments = cmdArguments.Replace("{saveDir}", appServer.GridName);
                cmdArguments = cmdArguments.Replace("{rconEnabled}", appServer.RconEnabled.ToString());
                cmdArguments = cmdArguments.Replace("{maxPlayers}", appServer.GetMaxPlayers().ToString());
                cmdArguments = cmdArguments.Replace("{reservedSlots}", appServer.GetReservedSlots().ToString());
                cmdArguments = cmdArguments.Replace("{reservedSlots}", appServer.GetReservedSlots().ToString());
                cmdArguments = cmdArguments.Replace("{rconPort}", appServer.GetRconPort().ToString());
                cmdArguments = cmdArguments.Replace("{queryPort}", appServer.GetQueryPort().ToString());
                cmdArguments = cmdArguments.Replace("{gamePort}", appServer.GetGamePort().ToString());
                cmdArguments = cmdArguments.Replace("{seamlessPort}", appServer.GetSeamlessPort().ToString());

                server.name             = appServer.Name;
                server.ip               = appServer.GetSeamlessIP();
                server.port             = appServer.GetQueryPort();
                server.gamePort         = appServer.GetGamePort();
                server.seamlessDataPort = appServer.GetSeamlessPort();
                server.isHomeServer     = appServer.IsHomeServer;

                await GameConfig.ServerGrid.UpdateAsync();

                using (var serverProccess = new Process())
                {
                    serverProccess.StartInfo.FileName = GameConfig.GetInstallDir() + GameConfig.SERVER_EXE;
                    serverProccess.StartInfo.UseShellExecute = true;
                    serverProccess.StartInfo.CreateNoWindow = false;

                    serverProccess.StartInfo.Arguments = cmdArguments;
                    if (serverProccess.Start())
                    {
                        using(SQLConnection sql = new())
                        {
                            appServer.PID = serverProccess.Id;
                            sql.AppServers.Update(appServer);
                            if(sql.SaveChangesAsync() == default)
                            {
                                Console.WriteLine($"{DateTime.UtcNow} [FAIL-UPDATE] appServer.PID({appServer.PID})");
                            }
                        }
                        Console.WriteLine($"{DateTime.UtcNow} [START] {server.GetGridName()}");
                    }
                }
            }

            return false;
        }

        public static string GetGridName(this Server server)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return $"{letters[server.gridX]}{server.gridY + 1}";
        }
        public static async Task StopServerAsync(this Server server)
        {
            var appServer = await server.CreateAppServerAsync();
            if(appServer is not null && appServer.PID is not null)
            {
                using (SQLConnection sql = new())
                {
                    try
                    {
                        var appServerProccess = Process.GetProcessById((int)appServer.PID);
                        appServerProccess.Kill();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    appServer.PID = null;
                    sql.AppServers.Update(appServer);
                    if(await sql.SaveChangesAsync() == 0)
                    {
                        Console.WriteLine($"{DateTime.UtcNow} [FAIL-UPDATE] appServer.PID({appServer.GridName})");
                        return;
                    }
                }

                Console.WriteLine($"{DateTime.UtcNow} [UPDATE] appServer.PID({appServer.GridName})");
            }
        }
    }
}

//    //{additionalArguements}
//-SeamlessPort
//-SeamlessLocalHost