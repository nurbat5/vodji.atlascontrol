using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Vodji.Atlas.AtlasCMD.ShooterGame.Models;

namespace Vodji.Atlas.AtlasCMD.ShooterGame
{
    public class ServerGrid_ServerOnly
    {
        public void GenerateDatabaseConnections()
        {
            var _password = Guid.NewGuid().GetHashCode().ToString();
            DatabaseConnections = new()
            {
                new DatabaseConnections("Default", "127.0.0.1", 6379, _password),
                new DatabaseConnections("TradeDB", "127.0.0.1", 6379, _password),
                new DatabaseConnections("TribeDB", "127.0.0.1", 6379, _password),
                new DatabaseConnections("TravelDataDB", "127.0.0.1", 6379, _password),
                new DatabaseConnections("TerritoryDB", "127.0.0.1", 6379, _password),
                new DatabaseConnections("LogDB", "127.0.0.1", 6379, _password)
            };
        }

        public string LocalS3URL { get; set; } = string.Empty;
        public string LocalS3AccessKeyId { get; set; } = string.Empty;
        public string LocalS3SecretKey { get; set; } = string.Empty;
        public string LocalS3BucketName { get; set; } = string.Empty;
        public string LocalS3Region { get; set; } = string.Empty;

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)] 
        public TribeLogConfig TribeLogConfig { get; set; } = new();
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)] 
        public SharedLogConfig SharedLogConfig { get; set; } = new();
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)] 
        public TravelDataConfig TravelDataConfig { get; set; } = new();
        public List<DatabaseConnections> DatabaseConnections { get; set; }
    }
}