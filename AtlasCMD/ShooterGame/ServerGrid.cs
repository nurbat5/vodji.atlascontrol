using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Vodji.Atlas.AtlasCMD.ShooterGame.Models;
using Vodji.Atlas.ServerGridEditor.Models;

namespace Vodji.Atlas.AtlasCMD.ShooterGame
{

    public class DeploymentAttribute : Attribute { }
    public class DeploymentOverrideAttribute : DeploymentAttribute { }
    public class DeploymentConstAttribute : DeploymentAttribute { }
    public class DeploymentOverrideShouldSerializeContractResolver : DefaultContractResolver
    {
        //public new static readonly ShouldSerializeContractResolver Instance = new ShouldSerializeContractResolver();

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);

            bool bShouldSersialize = member.GetCustomAttributes().OfType<DeploymentAttribute>().Any();
            property.ShouldSerialize =
                instance =>
                {
                    return bShouldSersialize;
                };

            return property;
        }
    }

    public class ServerGrid : ServerGrid_ServerOnly
    {
        // / <summary>
        // / -NoBattlEye
        // / -NoCrashDialog
        // / -culture=en
        // / -log
        // / </summary>
        // /         // public string BaseServerArgs { get; set; } = @"
        //     {mapName}{gridLocation}?
        //     AltSaveDirectoryName={saveDir}?
        //     MaxPlayers={maxPlayers}?
        //     ReservedPlayerSlots={reservedlots}?
        //     QueryPort={queryPort}?
        //     Port={port}?
        //     SeamlessIP={seamlessIP}?
        //     MapPlayerLocation=true
        //     {additionalArguements} -log -server -culture=en -NoCrashDialog ";

        [JsonIgnore]
        public ServerGrid_ServerOnly ServerOnly => this;

        [DeploymentOverride]
        public string BaseServerArgs { get; set; }
        public float gridSize { get; set; }
        [DeploymentOverride]
        public string MetaWorldURL { get; set; }
        [DeploymentOverride]
        public string WorldFriendlyName { get; set; }
        [DeploymentOverride]
        public string WorldAtlasId { get; set; }
        public string AuthListURL { get; set; }
        [DeploymentOverride]
        public string WorldAtlasPassword { get; set; }
        [DeploymentOverride]
        public string ModIDs { get; set; }
        [DeploymentOverride]
        public string MapImageURL { get; set; }
        public int totalGridsX { get; set; }
        public int totalGridsY { get; set; }
        public bool bUseUTCTime { get; set; }
        public float columnUTCOffset { get; set; }
        public string Day0 { get; set; }
        public float globalTransitionMinZ { get; set; }
        public string AdditionalCmdLineParams { get; set; }
        public Dictionary<string, string> OverrideShooterGameModeDefaultGameIni { get; set; } 
        public string globalGameplaySetup { get; set; }

        [DefaultValue(0.01f)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Include)]
        public float coordsScaling { get; set; }

        [DefaultValue(true)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Include)]
        public bool showServerInfo { get; set; }

        [DefaultValue(true)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Include)]
        public bool showDiscoZoneInfo { get; set; }

        [DefaultValue(true)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Include)]
        public bool showShipPathsInfo { get; set; }

        [DefaultValue(true)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Include)]
        public bool showIslandNames { get; set; }

        [DefaultValue(true)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool showLines { get; set; }

        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool alphaBackground { get; set; }

        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool showBackground { get; set; }

        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool showForeground = false;

        public string backgroundImgPath { get; set; }
        public string foregroundImgPath { get; set; }

        [DefaultValue("Resources/discoZoneBox.png")]
        public string discoZonesImagePath = null;

        [DeploymentConst]
        public List<Server> servers { get; set; }

        public List<SpawnerOverrideTemplate> spawnerOverrideTemplates { get; set; }

        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public int idGenerator { get; set; }

        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public int regionsIdGenerator { get; set; }

        [DefaultValue(0)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public int shipPathsIdGenerator;

        public List<ShipPath> shipPaths { get; set; }
        public DateTime lastImageOverride;
        public List<ServerTemplate> serverTemplates = new List<ServerTemplate>();

        public async Task UpdateDatabaseAsync() => await UpdateDatabaseAsync(GameConfig.GetInstallDir() + GameConfig.SERVER_ONLY);
        public async Task UpdateDatabaseAsync(string pathFile)
        {
            using (var memoryStream = new MemoryStream())
            {
                await System.Text.Json.JsonSerializer.SerializeAsync<ServerGrid_ServerOnly>(memoryStream, this, new System.Text.Json.JsonSerializerOptions
                {
                    WriteIndented = true
                });
                memoryStream.Position = 0;

                using (var streamReader = new StreamReader(memoryStream))
                {
                    await File.WriteAllTextAsync(pathFile, await streamReader.ReadToEndAsync());
                }
                Console.WriteLine($"[{DateTime.UtcNow}] Updated " + pathFile);
            }
        }



        public async Task UpdateAsync() => await UpdateAsync(GameConfig.GetInstallDir() + GameConfig.SERVER_GRID);
        public async Task UpdateAsync(string pathFile)
        {
            string JsonData = JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            await File.WriteAllTextAsync(pathFile, JsonData);
            Console.WriteLine($"[UPDATE-{Path.GetFileName(pathFile)}] {DateTime.UtcNow} {pathFile}");
        }

        public static async Task<ServerGrid> LoadAsync(string pathFile)
        {
            try
            {
                var jsonStr = await File.ReadAllTextAsync(pathFile);
                Console.WriteLine($"[{DateTime.UtcNow}] Loaded " + pathFile);
                return JsonConvert.DeserializeObject<ServerGrid>(jsonStr);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
                Environment.Exit(0);
            }

            return null;
        }

        //public async Task UpdateDatabaseAsync() => await UpdateAsync<ServerGrid_ServerOnly>(GameConfig.GetInstallDir() + GameConfig.SERVER_ONLY);
        //public async Task UpdateAsync() => await UpdateAsync<ServerGrid>(GameConfig.GetInstallDir() + GameConfig.SERVER_GRID);
        //public async Task UpdateAsync<T>(string pathFile) where T : class
        //{
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        await JsonSerializer.SerializeAsync<T>(memoryStream, this as T, new JsonSerializerOptions
        //        {
        //            WriteIndented = true
        //        });
        //        memoryStream.Position = 0;

        //        using (var streamReader = new StreamReader(memoryStream))
        //        {
        //            await File.WriteAllTextAsync(pathFile, await streamReader.ReadToEndAsync());
        //        }
        //        Console.WriteLine($"[{DateTime.UtcNow}] Updated " + pathFile);
        //    }
        //}

        //public void UpdateDatabase() => Update<ServerGrid_ServerOnly>(GameConfig.GetInstallDir() + GameConfig.SERVER_ONLY);
        //public void Update() => Update<ServerGrid>(GameConfig.GetInstallDir() + GameConfig.SERVER_GRID);
        //public void Update<T>(string pathFile) where T : class
        //{
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        JsonSerializer.SerializeAsync(memoryStream, this as T, new JsonSerializerOptions
        //        {
        //            WriteIndented = true
        //        }).Wait();
        //        memoryStream.Position = 0;

        //        using (var streamReader = new StreamReader(memoryStream))
        //        {
        //            File.WriteAllText(pathFile, streamReader.ReadToEndAsync().Result);
        //        }
        //        Console.WriteLine($"[{DateTime.UtcNow}] Updated " + pathFile);
        //    }
        //}

        //public static async Task<ServerGrid> LoadAsync(string pathFile)
        //{
        //    try
        //    {
        //        if (!File.Exists(pathFile)) return null;
        //        using (var fileStream = new FileStream(pathFile, FileMode.Open))
        //        {
        //            Console.WriteLine("[LOAD] " + pathFile);
        //            return await JsonSerializer.DeserializeAsync<ServerGrid>(fileStream);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        Console.ReadKey();
        //        Environment.Exit(0);
        //    }

        //    return null;
        //}
    }
}