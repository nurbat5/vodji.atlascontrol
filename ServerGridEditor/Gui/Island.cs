using System.Net.Mime;
namespace Vodji.Atlas.ServerGridEditor.Gui
{
    public class Island
    {
        // public string name;
        // public float x, y;
        // public string imagePath;
        // public int landscapeMaterialOverride = -1;
        // public List<string> sublevelNames;
        // public Dictionary<string, string> spawnerOverrides = new Dictionary<string, string>();
        // public List<string> extraSublevels;
        // public List<string> treasureMapSpawnPoints;
        // public List<string> wildPirateCampSpawnPoints;
        // public float minTreasureQuality = -1;
        // public float maxTreasureQuality = -1;
        // public bool useNpcVolumesForTreasures = false;
        // public bool useLevelBoundsForTreasures = true;
        // public bool prioritizeVolumesForTreasures = false;
        // public string islandTreasureBottleSupplyCrateOverrides = "";
        // public int islandPoints = 1;
        // public float singleSpawnPointX;
        // public float singleSpawnPointY;
        // public float singleSpawnPointZ;
        // public float maxIslandClaimFlagZ = 0.0f;
        // // [JsonIgnore]
        // public string modDir = null;

        // Image cachedImg = null;
        // Image cachedOptimizedImg = null;

        // public Image GetImage(bool optimized = false)
        // {
        //     if (cachedImg == null)
        //     {
        //         if (File.Exists(imagePath))
        //             cachedImg = Image.FromFile(imagePath);
        //         else
        //         {
        //             // MessageBox.Show("Could not find image (Setting to blank image):" + imagePath, "Missing Image Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //             Bitmap bmp = new Bitmap(78, 78);
        //             cachedImg = Image.FromHbitmap(bmp.GetHbitmap());
        //         }
        //     }

        //     if(optimized)
        //     {
        //         if (cachedOptimizedImg == null)
        //         {
        //             if (File.Exists(imagePath))
        //             {
        //                 cachedOptimizedImg = Image.FromFile(imagePath);
        //                 Bitmap bmp = new Bitmap(cachedOptimizedImg, cachedOptimizedImg.Width / 16, cachedOptimizedImg.Height / 16);
        //                 cachedOptimizedImg = (Image)bmp;
        //             }
        //         }

        //         if (cachedOptimizedImg != null)
        //             return cachedOptimizedImg;
        //     }

        //     return cachedImg;
        // }

        // public void InvalidateImage()
        // {
        //     Image.FromImage();
        //     if (cachedImg != null)
        //         cachedImg.Dispose();
        //     cachedImg = null;
        //     if (cachedOptimizedImg != null)
        //         cachedOptimizedImg.Dispose();
        //     cachedOptimizedImg = null;
        // }

        // public Island(string name, float x, float y, string imagePath, int landscapeMaterialOverride
        //     , List<string> sublevelNames, Dictionary<string, string> spawnerOverrides, List<string> treasureMapSpawnPoints, List<string> wildPirateCampSpawnPoints, float minTreasureQuality, float maxTreasureQuality, bool useNpcVolumesForTreasures,
        //     bool useLevelBoundsForTreasures, bool prioritizeVolumesForTreasures, string IslandTreasureBottleSupplyCrateOverrides, List<string> extraSublevels, int islandPoints, float sPlayerSpawnPointX, float sPlayerSpawnPointY, float sPlayerSpawnPointZ, float theMaxIslandClaimFlagZ)
        // {
        //     this.name = name;
        //     this.x = x;
        //     this.y = y;
        //     this.imagePath = imagePath;
        //     this.landscapeMaterialOverride = landscapeMaterialOverride;
        //     this.sublevelNames = sublevelNames;
        //     this.spawnerOverrides = spawnerOverrides;
        //     this.treasureMapSpawnPoints = treasureMapSpawnPoints;
        //     this.wildPirateCampSpawnPoints = wildPirateCampSpawnPoints;
        //     this.minTreasureQuality = minTreasureQuality;
        //     this.maxTreasureQuality = maxTreasureQuality;
        //     this.useNpcVolumesForTreasures = useNpcVolumesForTreasures;
        //     this.useLevelBoundsForTreasures = useLevelBoundsForTreasures;
        //     this.prioritizeVolumesForTreasures = prioritizeVolumesForTreasures;
        //     this.islandTreasureBottleSupplyCrateOverrides = IslandTreasureBottleSupplyCrateOverrides;
        //     this.extraSublevels = extraSublevels;
        //     this.islandPoints = islandPoints;
        //     this.singleSpawnPointX = sPlayerSpawnPointX;
        //     this.singleSpawnPointY = sPlayerSpawnPointY;
        //     this.singleSpawnPointZ = sPlayerSpawnPointZ;
        //     this.maxIslandClaimFlagZ = theMaxIslandClaimFlagZ;
        // }
    }
}