﻿namespace Vodji.Atlas.ServerGridEditor.Models
{
    public class MoveableObjectData
    {
        public float worldX { get; set; }
        public float worldY { get; set; }
        public float rotation { get; set; }
    }
}
